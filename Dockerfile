FROM node:12-alpine as builder

WORKDIR /usr/src/app
COPY package.json package*.json ./
RUN npm ci
COPY . .
RUN npm run build

FROM node:12-alpine

ENV PORT 8080
EXPOSE 8080

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

WORKDIR /usr/src/app
COPY --from=builder \
    /usr/src/app/package.json /usr/src/app/package*.json \
    /usr/src/app/node_modules /usr/src/app/build \
    /usr/src/app/src /usr/src/app/LICENSE.txt \
    /usr/src/app/

CMD [ "npm", "start" ]
