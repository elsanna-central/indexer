/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { HttpResponse } from '../../../src/http';
import { Index, IndexStories, StorySource } from '../../../src/sources';
import { FFNetSource, InvalidTagError } from '../../../src/sources/fanfictionNet';
import * as multiPageIndex from './samples/multiPageIndex';
import * as singlePageIndex from './samples/singlePageIndex';
import * as twoPageIndex from './samples/twoPageIndex';

describe('fanfiction.net', () => {
    const mockClient = {
        get: jest.fn(),
    };
    const downloader: StorySource = new FFNetSource(mockClient);
    describe('index downloader', () => {
        it('rejects on failure', async () => {
            mockClient.get.mockRejectedValueOnce(new Error('unexpected failure'));
            const response = downloader.getStoriesMetadata(Index.ELSANNA);
            await expect(response).rejects.toThrow('unexpected failure');

            expect(mockClient.get).toBeCalledTimes(1);
        });
        it('rejects invalid tag', () => {
            const call = () => FFNetSourceUnprotected
                .parseDescriptionTag('A invalid tag');
            expect(call).toThrow(new InvalidTagError('A invalid tag'));
        });

        test('single page index', async () => {
            mockClient.get
                .mockImplementationOnce((): Promise<HttpResponse<string>> => {
                    return Promise.resolve({
                        status: 200,
                        data: singlePageIndex.HTML,
                        headers: {},
                    });
                });

            const response = downloader.getStoriesMetadata(Index.ELSANNA);
            await expect(response).resolves.toEqual<IndexStories>(singlePageIndex.response);

            expect(mockClient.get).toBeCalledTimes(1);
            expect(mockClient.get).toHaveBeenNthCalledWith(1,
                'https://www.fanfiction.net/movie/Frozen/?&srt=1&lan=1&r=10&c1=108313&c2=108312&pm=1&p=1',
                {},
            );
        });

        test('two page index', async () => {
            twoPageIndex.HTMLs.forEach((content: string) => mockClient.get
                .mockImplementationOnce((): Promise<HttpResponse<string>> => {
                    return Promise.resolve({
                        status: 200,
                        data: content,
                        headers: {},
                    });
                }));

            const response = downloader.getStoriesMetadata(Index.ELSANNA);
            await expect(response).resolves.toEqual<IndexStories>(twoPageIndex.response);

            expect(mockClient.get).toBeCalledTimes(2);
            for (let i = 1; i <= 2; i++) {
                expect(mockClient.get).toHaveBeenNthCalledWith(i,
                    `https://www.fanfiction.net/movie/Frozen/?&srt=1&lan=1&r=10&c1=108313&c2=108312&pm=1` +
                    `&p=${(i).toFixed(0)}`, {},
                );
            }
        });

        test('multi page index', async () => {
            multiPageIndex.HTMLs.forEach((content: string, index: number) => mockClient.get
                .mockImplementationOnce((url: string): Promise<any> => {
                    expect(url).toBe(
                        `https://www.fanfiction.net/movie/Frozen/?&srt=1&lan=1&r=10&c1=108313&c2=108312&pm=1` +
                        `&p=${(index + 1).toFixed(0)}`,
                    );
                    return Promise.resolve({
                        status: 200,
                        data: content,
                    });
                }));

            const response = downloader.getStoriesMetadata(Index.ELSANNA);
            await expect(response).resolves.toEqual<IndexStories>(multiPageIndex.response);

            expect(mockClient.get).toBeCalledTimes(4);
        });
    });
});

class FFNetSourceUnprotected extends FFNetSource {
    public static parseDescriptionTag(tag: string) {
        return super.parseDescriptionTag(tag);
    }
}
