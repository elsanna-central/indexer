/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { readFileSync } from 'fs';
import * as moment from 'moment';
import { AudienceRating, Index, IndexStories } from '../../../../src/sources';
import { FFNetSource } from '../../../../src/sources/fanfictionNet/storySource';

export const HTMLs: string[] = new Array(4).fill('')
    .map((_, index: number): string => readFileSync(
        `${__dirname}/multiPageIndex_${(index + 1).toFixed(0)}.html`,
        {encoding: 'utf8'},
    ));

const dateMoment = (year: number, month: number, day: number) =>
    moment.utc([year, month - 1, day]).toDate();

export const response: IndexStories = {
    index: Index.ELSANNA,
    stories: [
        {
            sourceReference: FFNetSource.referenceId,
            title: 'Witness Protection',
            author: 'ShipperOfGayGirls',
            url: 'https://www.fanfiction.net/s/12088757/1/Witness-Protection',
            chapterCount: 10,
            audienceRating: AudienceRating.M,
            tags: new Set(['supernatural', 'romance']),
            complete: false,
            reviews: 80,
            wordCount: 60_046,

            publishedDate: dateMoment(2016, 8, 6),
            lastUpdate: dateMoment(2020, 4, 8),
        },
        {
            sourceReference: FFNetSource.referenceId,
            title: 'The Thief and the Frozen Heart',
            author: 'MegaTank',
            url: 'https://www.fanfiction.net/s/11515588/1/The-Thief-and-the-Frozen-Heart',
            imageUrl: 'https://ff74.b-cdn.net/image/4237628/75/',
            chapterCount: 14,
            audienceRating: AudienceRating.M,
            tags: new Set(['romance', 'mystery']),
            complete: false,
            reviews: 45,
            wordCount: 90_010,

            publishedDate: dateMoment(2015, 9, 19),
            lastUpdate: dateMoment(2019, 2, 15),
        },
        {
            sourceReference: FFNetSource.referenceId,
            title: 'Polaris',
            author: 'kaiserklee',
            url: 'https://www.fanfiction.net/s/11788069/1/Polaris',
            imageUrl: 'https://ff74.b-cdn.net/image/3285606/75/',
            chapterCount: 12,
            audienceRating: AudienceRating.M,
            tags: new Set(['tragedy', 'fantasy']),
            complete: false,
            reviews: 52,
            wordCount: 69_300,

            publishedDate: dateMoment(2016, 2, 14),
            lastUpdate: dateMoment(2016, 5, 22),
        },
        {
            sourceReference: FFNetSource.referenceId,
            title: 'She Fell From the Sky',
            author: 'AloverOfBooks',
            url: 'https://www.fanfiction.net/s/10564818/1/She-Fell-From-the-Sky',
            imageUrl: 'https://ff74.b-cdn.net/image/1608585/75/',
            chapterCount: 29,
            audienceRating: AudienceRating.M,
            tags: new Set(['mystery', 'fantasy']),
            complete: false,
            reviews: 349,
            wordCount: 132_087,

            publishedDate: dateMoment(2014, 7, 25),
            lastUpdate: dateMoment(2014, 11, 5),
        },
    ],
};
