/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { readFileSync } from 'fs';
import * as moment from 'moment';
import { AudienceRating, Index, IndexStories } from '../../../../src/sources';
import { FFNetSource } from '../../../../src/sources/fanfictionNet/storySource';

export const HTML: string = readFileSync(
    `${__dirname}/singlePageIndex.html`,
    {encoding: 'utf8'},
);

const dateMoment = (year: number, month: number, day: number) =>
    moment.utc([year, month - 1, day]).toDate();

export const response: IndexStories = {
    index: Index.ELSANNA,
    stories: [
        {
            sourceReference: FFNetSource.referenceId,
            title: 'The Queen\'s Mercy',
            author: 'JYN044',
            url: 'https://www.fanfiction.net/s/10910214/1/The-Queen-s-Mercy',
            imageUrl: 'https://ff74.b-cdn.net/image/3491793/75/',
            chapterCount: 99,
            audienceRating: AudienceRating.M,
            tags: new Set(['romance', 'adventure']),
            complete: true,
            reviews: 3151,
            wordCount: 464_722,

            publishedDate: dateMoment(2014, 12, 23),
            lastUpdate: dateMoment(2019, 10, 19),
        },
        {
            sourceReference: FFNetSource.referenceId,
            title: 'Drage Hjerte',
            author: 'Burning Phoenix X-7',
            url: 'https://www.fanfiction.net/s/11021463/1/Drage-Hjerte',
            imageUrl: 'https://ff74.b-cdn.net/image/1731673/75/',
            chapterCount: 34,
            audienceRating: AudienceRating.M,
            tags: new Set(['romance', 'adventure']),
            complete: true,
            reviews: 463,
            wordCount: 244_531,

            publishedDate: dateMoment(2015, 2, 4),
            lastUpdate: dateMoment(2019, 9, 30),
        },
        {
            sourceReference: FFNetSource.referenceId,
            title: 'The Last Knydaxian: The Series',
            author: 'Cybercitizen',
            url: 'https://www.fanfiction.net/s/12585614/1/The-Last-Knydaxian-The-Series',
            imageUrl: 'https://ff74.b-cdn.net/image/5397739/75/',
            chapterCount: 29,
            audienceRating: AudienceRating.MA,
            tags: new Set(['sci-fi', 'adventure']),
            complete: true,
            reviews: 6,
            wordCount: 108_541,

            publishedDate: dateMoment(2017, 7, 24),
            lastUpdate: dateMoment(2017, 11, 24),
        },
    ],
};
