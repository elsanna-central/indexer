/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { readFileSync } from 'fs';
import * as moment from 'moment';
import { AudienceRating, Index, IndexStories } from '../../../../src/sources';
import { FFNetSource } from '../../../../src/sources/fanfictionNet/storySource';


export const HTMLs: string[] = new Array(2).fill('')
    .map((_, index: number): string => readFileSync(
        `${__dirname}/twoPageIndex_${(index + 1).toFixed(0)}.html`,
        {encoding: 'utf8'},
    ));

const dateMoment = (year: number, month: number, day: number) =>
    moment.utc([year, month - 1, day]).toDate();

export const response: IndexStories = {
    index: Index.ELSANNA,
    stories: [
        {
            sourceReference: FFNetSource.referenceId,
            title: 'Risky Business',
            author: 'Fruipit',
            url: 'https://www.fanfiction.net/s/11649709/1/Risky-Business',
            imageUrl: 'https://ff74.b-cdn.net/image/2786601/75/',
            chapterCount: 111,
            audienceRating: AudienceRating.M,
            tags: new Set(['romance']),
            complete: true,
            reviews: 789,
            wordCount: 135_693,

            publishedDate: dateMoment(2015, 12, 4),
            lastUpdate: dateMoment(2020, 3, 15),
        },
        {
            sourceReference: FFNetSource.referenceId,
            title: 'Expect The Unexpected',
            author: 'Nicole The Dragon Rider',
            url: 'https://www.fanfiction.net/s/11383331/1/Expect-The-Unexpected',
            imageUrl: 'https://ff74.b-cdn.net/image/2455253/75/',
            chapterCount: 44,
            audienceRating: AudienceRating.M,
            tags: new Set(['romance', 'drama']),
            complete: true,
            reviews: 268,
            wordCount: 116_667,

            publishedDate: dateMoment(2015, 7, 16),
            lastUpdate: dateMoment(2016, 3, 11),
        },
    ],
};
