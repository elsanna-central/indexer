/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { readFileSync } from 'fs';
import * as moment from 'moment';
import { AudienceRating, Index, IndexStories } from '../../../../src/sources';
import { AO3Source } from '../../../../src/sources/ao3';

export const HTMLs: string[] = new Array(2).fill('')
    .map((_, index: number): string => readFileSync(
        `${__dirname}/multiPageIndex_${(index + 1).toFixed(0)}.html`,
        {encoding: 'utf8'},
    ));

const dateMoment = (year: number, month: number, day: number) =>
    moment.utc([year, month - 1, day]).toDate();

export const response: IndexStories = {
    index: Index.ELSANNA,
    stories: [
        {
            sourceReference: AO3Source.referenceId,
            title: 'To Be A Queen And A Wife',
            author: 'Cybercitizen',
            url: 'https://archiveofourown.org/works/23952916',
            chapterCount: 1,
            audienceRating: AudienceRating.T,
            tags: new Set([
                'Alternate Universe - Canon Divergence',
                'Canon',
                'Fluff',
                'Weddings',
                'Marriage',
                'Incest',
                'Love',
                'Cute',
                'Coronation',
                'Anna and Elsa are Siblings (Disney)',
            ]),
            complete: true,
            reviews: 2,
            wordCount: 3450,

            // publishedDate: dateMoment(2020, 5, 1),
            publishedDate: moment.utc(0).toDate(),
            lastUpdate: dateMoment(2020, 5, 1),
        },
        {
            sourceReference: AO3Source.referenceId,
            title: 'Jealousy Leads To Love',
            author: 'Cybercitizen',
            url: 'https://archiveofourown.org/works/14349114',
            chapterCount: 1,
            audienceRating: AudienceRating.MA,
            tags: new Set([
                'Smut',
                'Canon',
                'Cute',
                'Strap-On',
                'ice dildo',
                'Jealousy',
                'PWP',
            ]),
            complete: true,
            reviews: 0,
            wordCount: 1748,

            publishedDate: moment.utc(0).toDate(),
            lastUpdate: dateMoment(2018, 4, 17),
        },
    ],
};
