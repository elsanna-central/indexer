/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { HttpResponse } from '../../../src/http';
import { Index, IndexStories, StorySource } from '../../../src/sources';
import { AO3Source } from '../../../src/sources/ao3';
import * as multiPageIndex from './samples/multiPageIndex';
import * as singlePageIndex from './samples/singlePageIndex';

describe('AO3', () => {
    const mockClient = {
        get: jest.fn(),
    };
    const downloader: StorySource = new AO3Source(mockClient);
    describe('index downloader', () => {
        it('rejects on failure', async () => {
            mockClient.get.mockRejectedValueOnce(new Error('unexpected failure'));
            const response = downloader.getStoriesMetadata(Index.ELSANNA);
            await expect(response).rejects.toThrow('unexpected failure');

            expect(mockClient.get).toBeCalledTimes(1);
        });

        test('single page index', async () => {
            mockClient.get
                .mockImplementationOnce((): Promise<HttpResponse<string>> => {
                    return Promise.resolve({
                        status: 200,
                        data: singlePageIndex.HTML,
                        headers: {},
                    });
                });

            const response = downloader.getStoriesMetadata(Index.ELSANNA);
            await expect(response).resolves.toEqual<IndexStories>(singlePageIndex.response);

            expect(mockClient.get).toBeCalledTimes(1);
            expect(mockClient.get).toHaveBeenNthCalledWith(1,
                'https://archiveofourown.org/works?' +
                '&work_search%5Brelationship_ids%5D%5B%5D=1160471' +
                '&work_search%5Blanguage_id%5D=en&' +
                'tag_id=Frozen+%28Disney+Movies%29&page=1',
                {'Cookie': 'view_adult=true;'},
            );
        });

        test('multi page index', async () => {
            multiPageIndex.HTMLs.forEach((content: string) => mockClient.get
                .mockImplementationOnce((): Promise<HttpResponse<string>> => {
                    return Promise.resolve({
                        status: 200,
                        data: content,
                        headers: {},
                    });
                }));

            const response = downloader.getStoriesMetadata(Index.ELSANNA);
            await expect(response).resolves.toEqual<IndexStories>(multiPageIndex.response);

            expect(mockClient.get).toBeCalledTimes(2);
            for (let i = 1; i <= multiPageIndex.HTMLs.length; i++) {
                expect(mockClient.get).toHaveBeenNthCalledWith(i,
                    'https://archiveofourown.org/works?' +
                    '&work_search%5Brelationship_ids%5D%5B%5D=1160471' +
                    '&work_search%5Blanguage_id%5D=en' +
                    `&tag_id=Frozen+%28Disney+Movies%29&page=${i.toFixed(0)}`,
                    {'Cookie': 'view_adult=true;'},
                );
            }
        });
    });
});
