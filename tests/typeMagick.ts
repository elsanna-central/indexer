/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { MaybeMocked } from 'ts-jest/dist/util/testing';

type Callable = (...args: any[]) => any
type PartialCallable<T extends Callable> =
    (...args: Parameters<T>) => Partial<ReturnType<T>>

type Constructor = new(...args: any[]) => any
type PartialConstructor<T extends Constructor> =
    new(...args: ConstructorParameters<T>) => PartialMockedObject<InstanceType<T>>

type PartialMockedObject<T> = { [K in keyof T]?: PartialMocked<T[K]> }
export type PartialMocked<T> =
    (T extends Callable ? MaybeMocked<PartialCallable<T>> : {}) &
    (T extends Constructor ? MaybeMocked<PartialConstructor<T>> : {}) &
    (T extends object ? PartialMockedObject<T> : T)

export function partialMocked<T>(obj: T): PartialMocked<T> {
    return obj as any;
}
