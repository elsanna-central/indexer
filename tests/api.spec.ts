/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { getMockReq, getMockRes } from '@jest-mock/express';
import { AlgoliaEnvOptions, Api, MissingVariableError } from '../src/api';
import { SourceCombiner } from '../src/combiner';
import { AxiosHttpClient } from '../src/http';
import { IndexOperationResult, StoryIndexer } from '../src/indexers';
import { AlgoliaIndexer } from '../src/indexers/algolia';
import { Index, IndexStories, StorySource } from '../src/sources';
import { AO3Source } from '../src/sources/ao3';
import { FFNetSource } from '../src/sources/fanfictionNet';
import { partialMocked } from './typeMagick';

jest.mock('../src/indexers/algolia');
jest.mock('../src/combiner');
jest.mock('../src/sources/fanfictionNet');
jest.mock('../src/sources/ao3');
jest.mock('../src/http');

describe('Api controller', () => {
    it('throws on missing variables', () => {
        expect(Api.fromEnv).toThrow(MissingVariableError);
    });
    it('builds from environment', () => {
        process.env[AlgoliaEnvOptions.indexPrefix] = 'unit_test';
        process.env[AlgoliaEnvOptions.appId] = 'appId';
        process.env[AlgoliaEnvOptions.apiKey] = 'apiKey';
        expect(Api.fromEnv).not.toThrow();
        expect(partialMocked(AlgoliaIndexer)).lastCalledWith('unit_test', 'appId', 'apiKey');

        expect(partialMocked(AxiosHttpClient)).lastCalledWith();
        const httpClient = partialMocked(AxiosHttpClient).mock.instances[0];

        expect(partialMocked(FFNetSource)).lastCalledWith(httpClient);
        const ffNetSource = partialMocked(FFNetSource).mock.instances[0];

        expect(partialMocked(AO3Source)).lastCalledWith(httpClient);
        const ao3Source = partialMocked(AO3Source).mock.instances[0];

        expect(partialMocked(SourceCombiner)).lastCalledWith(ffNetSource, ao3Source);
    });
    it('index stories', async () => {
        const source = new FakeSource();
        const indexer = new FakeIndexer();
        const api = new Api(source, indexer);
        const {res: resMock} = getMockRes();

        source.getStoriesMetadata.mockImplementationOnce(index => Promise.resolve({
            index,
            stories: [],
        }));
        indexer.indexAll.mockResolvedValue({});

        const result = api.entrypoint(
            getMockReq<Parameters<typeof api.entrypoint>[0]>({
                body: {
                    index: 'ELSANNA',
                },
            }),
            resMock,
        );
        await expect(result).resolves.not.toBeDefined();
        expect(source.getStoriesMetadata).lastCalledWith(Index.ELSANNA);
        expect(indexer.indexAll).lastCalledWith({
            index: Index.ELSANNA,
            stories: [],
        });
        expect(resMock.status).lastCalledWith(200);
        expect(resMock.json).lastCalledWith({
            indexedStories: 0,
            message: 'Indexed successfully',
        });
    });
    it('fails when sourcing operation fails', async () => {
        const source = new FakeSource();
        const indexer = new FakeIndexer();
        const api = new Api(source, indexer);
        const {res: resMock} = getMockRes();

        source.getStoriesMetadata.mockRejectedValueOnce(new Error('mock error'));

        const result = api.entrypoint(
            getMockReq<Parameters<typeof api.entrypoint>[0]>({
                body: {index: 'ELSANNA'},
            }),
            resMock,
        );
        await expect(result).resolves.not.toBeDefined();
        expect(indexer.indexAll).not.toBeCalled();
        expect(resMock.status).lastCalledWith(500);
        expect(resMock.json).lastCalledWith({
            error: 'mock error',
        });
    });
    it('rejects unknown index names', async () => {
        const source = new FakeSource();
        const indexer = new FakeIndexer();
        const api = new Api(source, indexer);
        const {res: resMock} = getMockRes();

        source.getStoriesMetadata.mockRejectedValueOnce(new Error('mock error'));

        const result = api.entrypoint(
            getMockReq<Parameters<typeof api.entrypoint>[0]>({
                body: {index: 'UNKNOWN INDEX'},
            }),
            resMock,
        );
        await expect(result).resolves.not.toBeDefined();
        expect(indexer.indexAll).not.toBeCalled();
        expect(resMock.status).lastCalledWith(500);
        expect(resMock.json).lastCalledWith({
            error: 'Unknown index',
        });
    });
});

class FakeSource implements StorySource {
    public referenceId = 'Unused ID';
    public getStoriesMetadata = jest.fn<Promise<IndexStories>, [Index]>();
}

class FakeIndexer extends StoryIndexer {
    public indexAll = jest.fn<Promise<IndexOperationResult>, [IndexStories]>();
}
