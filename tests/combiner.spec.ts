/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { SourceCombiner } from '../src/combiner';
import { AudienceRating, Index, IndexStories, StoryMetadata, StorySource } from '../src/sources';

describe('Source combiner', () => {
    it('fails to combine 0 sources', () => {
        expect(() => new SourceCombiner()).toThrow();
    });
    it('combines all stories from multiple sources', async () => {
        const sourceOne = new MockSource(
            'source1',
            {
                sourceReference: 'source1',
                title: 'Dummy Story 1',
                url: 'https://fakeUrl/dummy1',
                audienceRating: AudienceRating.T,
                chapterCount: 1,
                complete: false,
                lastUpdate: new Date(2020, 3, 11),
                publishedDate: new Date(2020, 0, 1),
                tags: new Set(['dummy']),
            },
        );
        const sourceTwo = new MockSource(
            'source2',
            {
                sourceReference: 'source2',
                title: 'Dummy Story 2',
                url: 'https://fakeUrl/dummy2',
                audienceRating: AudienceRating.K,
                chapterCount: 2,
                complete: false,
                lastUpdate: new Date(2020, 3, 11),
                publishedDate: new Date(2020, 0, 1),
                tags: new Set(['dummy']),
            },
        );
        const combiner: StorySource = new SourceCombiner(sourceOne, sourceTwo);
        const got = await combiner.getStoriesMetadata(Index.ELSANNA);

        expect(got).toEqual<IndexStories>({
            index: Index.ELSANNA,
            stories: sourceOne.stories.concat(sourceTwo.stories),
        });
        expect(sourceOne.getStoriesMetadata).toBeCalledTimes(1);
        expect(sourceOne.getStoriesMetadata).toHaveBeenLastCalledWith(Index.ELSANNA);
        expect(sourceTwo.getStoriesMetadata).toBeCalledTimes(1);
        expect(sourceTwo.getStoriesMetadata).toHaveBeenLastCalledWith(Index.ELSANNA);
    });
    it('ignores errors when not strict (default)', async () => {
        const source = new MockSource('source');
        source.getStoriesMetadata.mockRejectedValueOnce('mocked error');
        const combiner: StorySource = new SourceCombiner(source);
        const got = await combiner.getStoriesMetadata(Index.ELSANNA);
        expect(got).toEqual<IndexStories>({index: Index.ELSANNA, stories: []});
    });
    it('passes errors when strict', async () => {
        const source = new MockSource('source');
        source.getStoriesMetadata.mockRejectedValueOnce('mocked error');
        const combiner: StorySource = (new SourceCombiner(source))
            .setStrict(true);
        const got = combiner.getStoriesMetadata(Index.ELSANNA);
        await expect(got).rejects.toEqual('mocked error');
    });
});

class MockSource implements StorySource {
    readonly referenceId: string;
    readonly stories: StoryMetadata[];
    getStoriesMetadata = jest.fn((index: Index) =>
        Promise.resolve<IndexStories>({
            index,
            stories: this.stories,
        }));

    constructor(id: string, ...stories: StoryMetadata[]) {
        this.referenceId = id;
        this.stories = stories;
    }
}
