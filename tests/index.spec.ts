/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { Api } from '../src/api';

describe('Index', () => {
        const sentinel = {};
        const envContructorSpy = jest.spyOn(Api, 'fromEnv')
            .mockReturnValue({
                entrypoint: sentinel,
            } as any);
        it('instantiated everything', async () => {
            const index = await import('../src/index');
            expect(envContructorSpy).toBeCalledTimes(1);
            expect(index.entrypoint).toBe(sentinel);
        });
    },
);
