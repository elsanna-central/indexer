/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { WaitablePromise } from '@algolia/client-common';
import { SearchClient, SearchIndex } from 'algoliasearch';
import { IndexOperationResult } from '../../src/indexers';
import { AlgoliaIndexer } from '../../src/indexers/algolia';
import { AudienceRating, Index, Sources } from '../../src/sources';
import { PartialMocked } from '../typeMagick';

describe('Algolia indexer', () => {
    const indexMock: PartialMocked<SearchIndex> = {
        saveObjects: jest.fn(),
    };
    const clientMock: PartialMocked<SearchClient> = {
        initIndex: jest.fn().mockReturnValue(indexMock),
    };
    const algoliaMock = jest.spyOn(AlgoliaIndexer, 'clientBuilder')
        .mockReturnValue(clientMock as SearchClient);
    it('sends a whole index metadata to algolia', async () => {
        const indexer = new AlgoliaIndexer('unit_test', 'appId', 'apikey');
        expect(algoliaMock).toBeCalledTimes(1);
        indexMock.saveObjects?.mockReturnValue(makeWaitable({
            taskIDs: [1, 2],
            objectIDs: [],
        }));

        const result = indexer.indexAll({
            index: Index.ELSANNA,
            stories: [
                {
                    sourceReference: Sources.FanFictionNet,
                    title: 'Some story',
                    author: 'Amazing Author',
                    url: 'https://myUrl',
                    audienceRating: AudienceRating.T,
                    chapterCount: 100,
                    tags: new Set(['some', 'tags']),
                    complete: true,
                    publishedDate: new Date(2019, 6, 23),
                    lastUpdate: new Date(2020, 11, 30),
                },
            ],
        });
        await expect(result).resolves.toEqual<IndexOperationResult>({
            message: 'Stories indexed by task 1,2',
        });
        expect(clientMock.initIndex).toBeCalledTimes(1);
        expect(clientMock.initIndex).lastCalledWith('unit_test_ELSANNA');
        expect(indexMock.saveObjects).toBeCalledTimes(1);
        expect(indexMock.saveObjects).lastCalledWith(
            [
                {
                    sourceReference: Sources.FanFictionNet,
                    title: 'Some story',
                    author: 'Amazing Author',
                    uniqueKey: 'some story | amazing author',
                    url: 'https://myUrl',
                    objectID: 'https://myUrl',
                    audienceRating: 'T',
                    chapterCount: 100,
                    tags: ['some', 'tags'],
                    complete: true,
                    publishedDate: new Date(2019, 6, 23),
                    lastUpdate: new Date(2020, 11, 30),
                },
            ],
            {
                autoGenerateObjectIDIfNotExist: false,
            },
        );
    });
});

type ToWaitable<T> = T extends PromiseLike<infer R> ? WaitablePromise<R> : WaitablePromise<T>

function makeWaitable<T>(t: T): ToWaitable<T> {
    const p: any = Promise.resolve(t);
    p.wait = function () {
        return this;
    };
    return p;

}
