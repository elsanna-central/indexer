/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { IndexOperationResult, StoryIndexer } from '../../src/indexers';
import { AudienceRating, IndexStories, StoryMetadata } from '../../src/sources';

class TestIndexer extends StoryIndexer {
    public indexAll = jest.fn<Promise<IndexOperationResult>, [IndexStories]>()
        .mockResolvedValue({message: 'Mock called successfully'});

    public toExternalForm(metadata: StoryMetadata): ReturnType<StoryIndexer['toExternalForm']> {
        return super.toExternalForm(metadata);
    }
}

describe('Abstract indexer', () => {
    it('converts raw metadata to exportable formats', () => {
        const indexer = new TestIndexer();
        const got = indexer.toExternalForm({
            sourceReference: 'A Source',
            title: 'Sample story',
            author: 'Some author',
            url: 'https://myUrl',
            audienceRating: AudienceRating.T,
            complete: false,

            characters: new Set(['janet', 'carl', 'bob']),
            pairings: new Set([
                new Set(['janet', 'carl']),
            ]),
            imageUrl: 'https://myUrl/image',
            tags: new Set(['some', 'tags']),
            chapterCount: 100,
            reviews: 34,
            wordCount: 43,

            lastUpdate: new Date(2019, 6, 23),
            publishedDate: new Date(2019, 11, 30),
        });

        expect(got).toEqual({
            sourceReference: 'A Source',
            title: 'Sample story',
            author: 'Some author',
            url: 'https://myUrl',
            audienceRating: 'T',
            complete: false,

            characters: ['janet', 'carl', 'bob'],
            pairings: [
                ['janet', 'carl'],
            ],
            imageUrl: 'https://myUrl/image',
            tags: ['some', 'tags'],
            chapterCount: 100,
            reviews: 34,
            wordCount: 43,

            lastUpdate: new Date(2019, 6, 23),
            publishedDate: new Date(2019, 11, 30),
        });
    });
});
