/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import axios, { AxiosResponse } from 'axios';
import rateLimit from 'axios-rate-limit';

import { AxiosHttpClient } from '../src/http';
import { partialMocked } from './typeMagick';

jest.mock('axios-rate-limit');
describe('Axios HTTP client', () => {
    const axiosInstanceGet = jest.fn<Promise<AxiosResponse>, any[]>();
    const axiosCreate = jest.spyOn(axios, 'create')
        .mockImplementation((): any => ({
            get: axiosInstanceGet,
        }));
    const rateLimitMock = partialMocked(rateLimit).mockImplementation(c => c);
    it('forwards get requests unchanged', async () => {
        const result = {
            config: {},
            data: 'result',
            headers: {'some': 'headers'},
            status: 200,
            statusText: 'OK',
        };
        axiosInstanceGet.mockResolvedValueOnce(result);

        const url = 'something';
        const headers = {
            'My-Header': 'value',
        };

        const client = new AxiosHttpClient();
        await expect(client.get(url, headers)).resolves.toEqual(result);

        expect(axiosCreate).toBeCalledTimes(1);
        expect(rateLimitMock).toBeCalledTimes(1);
        expect(axiosInstanceGet).toBeCalledTimes(1);
        expect(axiosInstanceGet).lastCalledWith(url, {headers});
    });
});
