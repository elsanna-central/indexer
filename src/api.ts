/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { SourceCombiner } from './combiner';
import { AxiosHttpClient } from './http';
import { StoryIndexer } from './indexers';
import { AlgoliaIndexer } from './indexers/algolia';
import { Index, StorySource } from './sources';
import { AO3Source } from './sources/ao3';
import { FFNetSource } from './sources/fanfictionNet';

export enum AlgoliaEnvOptions {
    indexPrefix = 'ALGOLIA_INDEX_PREFIX',
    appId       = 'ALGOLIA_APP_ID',
    apiKey      = 'ALGOLIA_API_KEY',
}

export class Api {
    private readonly source: StorySource;
    private readonly indexer: StoryIndexer;

    constructor(
        source: StorySource,
        indexer: StoryIndexer,
    ) {
        this.source = source;
        this.indexer = indexer;
        this.entrypoint = this.entrypoint.bind(this);
    }

    public static fromEnv(): Api {
        const client = new AxiosHttpClient();
        const source = new SourceCombiner(
            new FFNetSource(client),
            new AO3Source(client),
        );
        source.setStrict(false);
        const indexer = new AlgoliaIndexer(
            Api.requireEnv(AlgoliaEnvOptions.indexPrefix),
            Api.requireEnv(AlgoliaEnvOptions.appId),
            Api.requireEnv(AlgoliaEnvOptions.apiKey),
        );
        return new Api(source, indexer);
    }

    public static requireEnv(name: string): string {
        const value = process.env[name];
        if (value === undefined) throw new MissingVariableError(name);
        return value;
    }

    public async entrypoint(req: ApiRequest, res: ApiResponse) {
        const index = Index[req.body.index];
        if (index === undefined) {
            res.status(500).json({
                error: 'Unknown index',
            });
            return;
        }
        try {
            const indexMeta = await this.source.getStoriesMetadata(index);
            const indexOperationResponse = await this.indexer.indexAll(indexMeta);
            res.status(200).json({
                indexedStories: indexMeta.stories.length,
                message: indexOperationResponse.message || 'Indexed successfully',
            });
        } catch (e) {
            res.status(500).json({
                error: e.message,
            });
        }
    }
}

export class MissingVariableError extends Error {
    public readonly name = 'MissingVariableError';
    public readonly variable: string;

    constructor(name: string) {
        super(`Missing variable "${name}"`);
        this.variable = name;
    }
}

interface RequestBody {
    readonly index: keyof typeof Index
}

interface ResponseBodySuccess {
    indexedStories: number
    message: string
}

interface ResponseBodyError {
    error: string
}

type ResponseBody = ResponseBodySuccess | ResponseBodyError
type ApiRequest = Request<ParamsDictionary, ResponseBody, RequestBody>

type ApiResponse = Response<ResponseBody>
