/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

export class FFNetError extends Error {
    public name = 'FFNetError';
}

export class InvalidTagError extends FFNetError {
    public name = 'FFNetError - Invalid Tag';
    public tag: string;

    constructor(tag: string) {
        super(`Invalid description tag: ${tag}`);
        this.tag = tag;
    }
}
