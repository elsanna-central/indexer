/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import cheerio from 'cheerio';
import moment from 'moment';
import { URL } from 'url';
import { AudienceRating, Index, IndexStories, Sources, StoryMetadata } from '..';
import { AbstractCrawlerStorySource } from '../abstractCrawler';
import { FFNetError, InvalidTagError } from './errors';

const indexParameters: { [k in Index]: string } = {
    [Index.ELSANNA]: 'movie/Frozen/?&srt=1&lan=1&r=10&c1=108313&c2=108312&pm=1',
};

interface TagData {
    rate: string
    lang: string
    tags?: string
    chapters: string
    words: string
    reviews?: string
    complete: string | undefined
}

export class FFNetSource extends AbstractCrawlerStorySource {
    public static referenceId = Sources.FanFictionNet;
    public readonly referenceId = FFNetSource.referenceId;
    protected readonly baseUrl = 'https://www.fanfiction.net';
    protected readonly pageQueryParam = 'p';
    protected readonly indexUrlMap = indexParameters;

    public static parseDescriptionTag(tagText: string): TagData {
        const tagMatch = new RegExp(
            '^Rated: (?<rate>[A-Z\\+]+)' +
            '\\s+-\\s+(?<lang>\\w+)' +
            '(?:\\s+-\\s+(?<tags>[\\w\\/\\- ]+))?' +
            '\\s+-\\s+Chapters: (?<chapters>\\d+)' +
            '\\s+-\\s+Words: (?<words>[\\d,]+)' +
            '(?:\\s+-\\s+Reviews: (?<reviews>\\d+))?' +
            '.+?(?<complete>Complete)?$', 'gs',
        ).exec(tagText);

        if (!tagMatch || tagMatch.groups === undefined) {
            throw new InvalidTagError(tagText);
        }
        return tagMatch.groups as any;
    }

    private static parseDateSpan(dateSpan: CheerioElement): Date {
        return moment
            .utc(parseInt(dateSpan.attribs['data-xutime'], 10) * 1000)
            .startOf('day')
            .toDate();
    }

    public getStoriesMetadata(index: Index): Promise<IndexStories> {
        return super.getStoriesMetadata(index)
            .catch(reason => Promise.reject(new FFNetError(reason)));
    }

    protected extractStories(page: Cheerio): StoryMetadata[] {
        const storyDivs = page.find('div.z-list.zhover.zpointer');
        const result: StoryMetadata[] = [];

        storyDivs.each((_: number, elem: CheerioElement) => {
            const cheer = cheerio(elem);
            const dataDiv = cheer.find('div.z-padtop2.xgray');
            const tagText = dataDiv.text().trim();

            const groups = FFNetSource.parseDescriptionTag(tagText);

            const dateSpans = dataDiv.find('span');
            const updatedSpan = dateSpans[0];
            // One shots only have one date span
            const publishedSpan = dateSpans[dateSpans.length === 1 ? 0 : 1];

            const imageUrl = cheer.find('img.cimage').data('original');

            // TODO: Extract list of characters and pairings
            result.push({
                sourceReference: this.referenceId,

                title: cheer.find('a.stitle').text().replace(/\s+/g, ' '),
                author: cheer.find('a.reviews').prev().text().replace(/\s+/g, ' '),
                imageUrl: !!imageUrl ? `https:${imageUrl}` : undefined,
                url: this.baseUrl + cheer.find('a.stitle').attr('href'),

                chapterCount: parseInt(groups.chapters, 10),
                audienceRating: groups.rate as AudienceRating,
                tags: new Set((groups.tags ?? '').split('/').map(s => s.toLowerCase())),
                complete: groups.complete !== undefined,

                publishedDate: FFNetSource.parseDateSpan(publishedSpan),
                lastUpdate: FFNetSource.parseDateSpan(updatedSpan),
                reviews: parseInt(groups.reviews ?? '0', 10),
                wordCount: parseInt(groups.words.replace(',', ''), 10),
            });
        });
        return result;
    }

    protected extractPageCount(page: Cheerio): number {
        const lastPageLink = page
            .find('div.z-list.zhover.zpointer')
            .prevAll('center')
            .children('a:nth-last-child(2), a:nth-last-child(1)')
            .attr('href');
        if (!lastPageLink) {
            return 1;
        }
        const url = new URL(lastPageLink, 'https://fanfiction.net');
        return parseInt(url.searchParams.get('p') || '1', 10);
    }
}
