/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

/**
 * AudienceRating are the rating levels defined at https://www.fictionratings.com/
 */
export enum AudienceRating {
    K       = 'K',
    KPlus   = 'K+',
    T       = 'T',
    M       = 'M',
    MA      = 'MA',
    UNKNOWN = 'UNKNOWN',
}

export enum Index {
    ELSANNA,
}

export enum Sources {
    FanFictionNet = 'FanFiction NET',
    AO3           = 'Archive of our Own',
}

export interface IndexStories {
    readonly index: Index
    readonly stories: StoryMetadata[]
}

export interface StoryMetadata {
    readonly sourceReference: string
    readonly title: string
    readonly author?: string
    readonly url: string
    readonly audienceRating: AudienceRating
    readonly chapterCount: number
    readonly tags: Set<string>
    readonly complete: boolean

    readonly characters?: Set<string>
    readonly pairings?: Set<Set<string>>
    readonly imageUrl?: string
    readonly wordCount?: number
    readonly reviews?: number
    readonly publishedDate: Date
    readonly lastUpdate: Date
}

export interface StorySource {
    readonly referenceId: string

    getStoriesMetadata(index: Index): Promise<IndexStories>
}
