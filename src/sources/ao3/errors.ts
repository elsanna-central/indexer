/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

export class AO3Error extends Error {
    public name = 'AO3Error';
}
