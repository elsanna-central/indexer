/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import cheerio from 'cheerio';
import moment from 'moment';
import { AudienceRating, Index, IndexStories, Sources, StoryMetadata } from '..';
import { Headers } from '../../http';
import { AbstractCrawlerStorySource } from '../abstractCrawler';
import { AO3Error } from './errors';

const indexUrls = {
    [Index.ELSANNA]: 'works?&work_search%5Brelationship_ids%5D%5B%5D=1160471&work_search%5Blanguage_id%5D=en&tag_id=Frozen+%28Disney+Movies%29',
};

const tagBlacklist = new Set<string>([
    'Tags will be added',
    'Might have forgot something',
]);

const ratingMapping: { [k: string]: AudienceRating } = {
    'rating-general': AudienceRating.KPlus,
    'rating-teen': AudienceRating.T,
    'rating-mature': AudienceRating.M,
    'rating-explicit': AudienceRating.MA,
    'rating-notrated': AudienceRating.UNKNOWN,
};

export class AO3Source extends AbstractCrawlerStorySource {
    public static referenceId = Sources.AO3;
    public readonly referenceId = AO3Source.referenceId;
    protected readonly baseUrl = 'https://archiveofourown.org';
    protected readonly pageQueryParam = 'page';
    protected readonly indexUrlMap: { [k in Index]: string } = indexUrls;
    protected readonly requestHeaders: Headers = {
        'Cookie': 'view_adult=true;',
    };

    public async getStoriesMetadata(target: Index): Promise<IndexStories> {
        return super.getStoriesMetadata(target)
            .catch(reason => Promise.reject(new AO3Error(reason)));
    }

    protected extractStories(page: Cheerio): StoryMetadata[] {
        return page.find('ol.work.index.group > li.work.group').toArray()
            .map(elem => cheerio(elem))
            .map<StoryMetadata>((elem: Cheerio): StoryMetadata => {
                const title = elem.find('div.header > h4 > a').first();
                const author = elem.find('div.header > h4 > a[rel=author]').first();
                return {
                    sourceReference: this.referenceId,
                    title: title.text(),
                    author: author.text(),
                    url: this.baseUrl + title.attr('href')!,
                    audienceRating: this.getRating(elem),
                    chapterCount: this.getChapterCount(elem),
                    tags: this.getTags(elem),
                    complete: this.getCompleteness(elem),
                    // characters: undefined,
                    // pairings: undefined,
                    wordCount: this.getWordCount(elem),
                    reviews: this.getReviews(elem),
                    publishedDate: new Date(0),
                    lastUpdate: this.getUpdateTime(elem),
                };
            });
    }

    protected extractPageCount(page: Cheerio): number {
        return parseInt(
            page.find('ol.pagination.actions > li:nth-last-of-type(2)')
                .first()
                .text(),
            10);
    }

    protected getRating(elem: Cheerio): AudienceRating {
        const ratingSpan = elem.find('span.rating');
        for (const [k, v] of Object.entries(ratingMapping)) {
            if (ratingSpan.hasClass(k)) return v;
        }
        return AudienceRating.UNKNOWN;
    }

    protected getChapterCount(elem: Cheerio): number {
        return parseInt(
            elem.find('dd.chapters')
                .text()
                .split('/', 1)[0]
                .replace(/[^\d]/g, ''),
            10);
    }

    protected getTags(elem: Cheerio): Set<string> {
        return new Set<string>(
            elem.find('li.freeforms > a.tag').toArray()
                .map(tag => cheerio(tag).text().trim())
                .map(tag => tag
                    .replace('&amp;', 'and')
                    .replace('&', 'and'))
                .filter(tag => !tagBlacklist.has(tag)),
        );
    }

    protected getCompleteness(elem: Cheerio): boolean {
        return elem.find('span.iswip').hasClass('complete-yes');
    }

    protected getWordCount(elem: Cheerio): number {
        return parseInt(
            elem.find('dd.words')
                .text()
                .replace(/[^\d]/g, ''),
            10);
    }

    protected getReviews(elem: Cheerio): number {
        const parsed = parseInt(
            elem.find('dd.comments')
                .text()
                .replace(/[^\d]/g, ''),
            10);
        return isNaN(parsed) ? 0 : parsed;
    }

    protected getUpdateTime(elem: Cheerio): Date {
        return moment
            .utc(elem.find('p.datetime').text(), 'DD MMM YYYY')
            .startOf('day')
            .toDate();
    }
}
