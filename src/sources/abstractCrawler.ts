/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import cheerio from 'cheerio';
import { Headers, HttpClient } from '../http';
import { Index, IndexStories, StoryMetadata, StorySource } from './index';

export abstract class AbstractCrawlerStorySource implements StorySource {
    public readonly abstract referenceId: string;
    protected abstract readonly baseUrl: string;
    protected abstract readonly pageQueryParam: string;
    protected abstract readonly indexUrlMap: { [k in Index]: string };
    protected readonly requestHeaders: Headers = {};
    protected readonly client: HttpClient;

    constructor(client: HttpClient) {
        this.client = client;
    }

    public async getStoriesMetadata(target: Index): Promise<IndexStories> {
        const firstPage = await this.getPageOf(target, 1);
        const pageCount = this.extractPageCount(firstPage.root());

        const pendingPages: (StoryMetadata[] | Promise<StoryMetadata[]>)[] = [
            this.extractStories(firstPage.root()),
        ];
        if (pageCount > 1) {
            for (let pageNumber = 2; pageNumber <= pageCount; pageNumber++) {
                pendingPages.push(
                    this.getPageOf(target, pageNumber)
                        .then(page => this.extractStories(page.root())),
                );
            }
        }

        const stories = (await Promise.all<StoryMetadata[]>(pendingPages)).flat();
        console.debug(`Found ${stories.length} total stories on "${this.referenceId}"`);

        return {index: target, stories};
    }

    protected abstract extractStories(page: Cheerio): StoryMetadata[];

    protected abstract extractPageCount(page: Cheerio): number;

    protected getPageOf(target: Index, page: number): Promise<CheerioStatic> {
        return this.client
            .get<string>(
                `${this.baseUrl}/${this.indexUrlMap[target]}&${this.pageQueryParam}=${page.toFixed(0)}`,
                this.requestHeaders,
            )
            .then(res => cheerio.load(res.data));
    }
}
