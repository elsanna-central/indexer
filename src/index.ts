/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { HttpFunction } from '@google-cloud/functions-framework/build/src/functions';
import 'source-map-support/register';
import { Api } from './api';

export const entrypoint: HttpFunction = Api.fromEnv().entrypoint;
