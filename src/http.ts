/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import axios, { AxiosInstance } from 'axios';
import rateLimit from 'axios-rate-limit';
import * as http from 'http';
import * as https from 'https';

export interface Headers {[k: string]: string}

export interface HttpResponse<T> {
    data: T
    status: number
    headers: Headers
}

export interface HttpClient {
    get<T>(url: string, headers?: Headers): Promise<HttpResponse<T>>

    // post(url: string, body: string, headers?: Headers): Promise<HttpResponse>
}

export class AxiosHttpClient implements HttpClient {
    protected readonly axiosClient: AxiosInstance;

    constructor() {
        this.axiosClient = rateLimit(
            axios.create({
                httpAgent: new http.Agent({keepAlive: true}),
                httpsAgent: new https.Agent({keepAlive: true}),
            }),
            {
                maxRequests: 5,
                perMilliseconds: 1000,
            },
        );
    }

    public get<T>(url: string, headers?: Headers): Promise<HttpResponse<T>> {
        return this.axiosClient.get<T>(url, {headers});
    }
}
