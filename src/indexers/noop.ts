/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { IndexStories } from '../sources';
import { IndexOperationResult, StoryIndexer } from './index';

export class NoopIndexer extends StoryIndexer {
    public indexAll(_meta: IndexStories): Promise<IndexOperationResult> {
        return Promise.resolve({
            message: 'Nothing indexed. This is expensive, so we used a NoOp instead!',
        });
    }
}
