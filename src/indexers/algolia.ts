/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import algolia, { SearchClient } from 'algoliasearch';
import { Index, IndexStories } from '../sources';
import { IndexOperationResult, StoryIndexer } from './index';

export class AlgoliaIndexer extends StoryIndexer {
    public static clientBuilder = algolia;
    private readonly client: SearchClient;
    private readonly indexPrefix: string;

    constructor(indexPrefix: string, appId: string, apiKey: string) {
        super();
        this.indexPrefix = indexPrefix;
        this.client = AlgoliaIndexer.clientBuilder(appId, apiKey);
    }

    public indexAll(indexMeta: IndexStories): Promise<IndexOperationResult> {
        const index = this.client.initIndex(
            `${this.indexPrefix}_${Index[indexMeta.index]}`,
        );
        return index.saveObjects(
            indexMeta.stories
                .map(this.toExternalForm)
                .map(meta => ({
                    objectID: meta.url,
                    uniqueKey: this.uniqueIndex(meta.title as string, meta.author as string),
                    ...meta,
                })),
            {
                autoGenerateObjectIDIfNotExist: false,
            })
            .wait()
            .then(res => ({
                message: `Stories indexed by task ${res.taskIDs}`,
            }));
    }

    // noinspection JSMethodCanBeStatic
    private uniqueIndex(title: string, author: string): string {
        title = (title ?? '').trim().toLowerCase();
        author = (author ?? '').trim().toLowerCase();
        return `${title} | ${author}`;
    }
}
