/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { IndexStories, StoryMetadata } from '../sources';

type ExportableMetadata = { [k in keyof StoryMetadata]: {} };

export interface IndexOperationResult {
    readonly message?: string
}

// export class IndexOperationError extends Error {}

export abstract class StoryIndexer {
    private static convertSetToArray(s: Set<any>): any[] {
        return Array.from(s.values())
            .map(e => e instanceof Set ? StoryIndexer.convertSetToArray(e) : e);
    }

    public abstract indexAll(meta: IndexStories): Promise<IndexOperationResult>

    protected toExternalForm(metadata: StoryMetadata): ExportableMetadata {
        const response: any = metadata;
        for (const [k, v] of Object.entries(response)) {
            if (v instanceof Set) {
                response[k] = StoryIndexer.convertSetToArray(v);
            }
        }
        return response;
    }
}
