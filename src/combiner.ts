/*
 * Copyright (c) 2020.
 * Licensed under the Open Software License version 3.0 as described in LICENSE.txt
 */

import { Index, IndexStories, StoryMetadata, StorySource } from './sources';

export class SourceCombiner implements StorySource {
    public static referenceId = 'Source Combiner';
    public readonly referenceId = SourceCombiner.referenceId;
    private ignoreErrors = true;
    private readonly sources: StorySource[];

    constructor(...sources: StorySource[]) {
        if (sources.length === 0) {
            throw new Error('Combining 0 sources is impossible!');
        }
        this.sources = sources;
    }

    public async getStoriesMetadata(index: Index): Promise<IndexStories> {
        let stories: StoryMetadata[] = [];

        for (const downloader of this.sources) {
            try {
                const partial = await downloader.getStoriesMetadata(index);
                stories = stories.concat(partial.stories);
            } catch (e) {
                if (!this.ignoreErrors) throw e;
            }
        }

        return {index, stories};
    }

    public setStrict(strict: boolean): this {
        this.ignoreErrors = !strict;
        return this;
    }
}
